-- flatgen mod for 100% flat maps - created by Krock
-- License: WTFPL

local DIRT_Y = -1		-- Where dirt starts [1]
local DIRT_DEPTH = 3	-- Depth of dirt [3]

local AIR = minetest.get_content_id("air")
local STONE = minetest.get_content_id("default:stone")
local DIRT = minetest.get_content_id("default:dirt")
local DIRT_GRASS = minetest.get_content_id("default:dirt_with_grass")

minetest.register_on_mapgen_init(function(mgparams)
		minetest.set_mapgen_params({mgname="singlenode"})
end)

minetest.register_on_generated(function(minp, maxp, seed)
	if minp.y > DIRT_Y then
		return
	end

	local x0 = minp.x
	local x1 = maxp.x
	local y0 = minp.y
	local y1 = maxp.y
	local z0 = minp.z
	local z1 = maxp.z
	
	local vm, emin, emax = minetest.get_mapgen_object("voxelmanip")
	local area = VoxelArea:new{MinEdge=emin, MaxEdge=emax}
	local data = vm:get_data()
	
	for z = z0, z1 do -- north pole
	for y = y0, y1  do -- sky
		local vi = area:index(x0, y, z)
		for x = x0, x1 do
			if(y <= DIRT_Y - DIRT_DEPTH) then
				data[vi] = STONE
			elseif(y == DIRT_Y) then
				data[vi] = DIRT_GRASS
			elseif(y < DIRT_Y and y > DIRT_Y - DIRT_DEPTH) then
				data[vi] = DIRT
			else
				data[vi] = AIR
			end
			vi = vi + 1
		end
	end
	end
	
	vm:set_data(data)
	vm:set_lighting({day=0, night=0})
	vm:calc_lighting()
	vm:write_to_map(data)
end)
