Copyright (C) 2015-2017  Ruben Rodriguez <ruben@gnu.org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

## Install:

 * Install dependencies:

```apt-get install minetest gir1.2-webkit-3.0 gir1.2-gtk-3.0```

 * Download and run blockly-minetest:

```
git clone https://devel.trisquel.info/ruben/blockly-minetest.git
cd blockly-minetest
# Copy the mods into the minetest config directory. Only needed the first time
cp mods/* ~/minetest -a
python blockly-minetest.py
```

 * Run and configure Minetest
  * Launch minetest
  * Create a new world. Click on configure and enable the "commands" mod. If you want to start in an empty world, also enable the flatgen mod. Click Save and then Play. Enjoy!

## Usage:

 NOTE: I'll be calling Minetest blocks "bricks", to avoid confusion. By "Block" I'll mean a Blockly program block.

 The blockly-minetest interface is a standard Blockly canvas where you can drag and drop blocks. There are two specific Minetest blocks: the brick creator (with parameters material, int, int, int) and the material block, wich is a list of values. When the creator block is run, a Minetest brick of the selected material will appear at the x y z coordinates provided, or at 0,0,0 if not provided.

 * To make a brick dissapear, create a brick in that position with material "air".
 * To know the coordinates you are at in Minetest, press f5 and an informational text will appear on the top of the game.
 * You can "run" the program, which executes it in one go, or "run block by block" which adds a small delay between creating bricks so you can see the progress.
 * You can also "Convert to Python", which will render the current Blockly program into Python, which then you can edit or run from the "Python code" tab. This part is experimental. Each time you press "Convert to Python" the content of the Python editor will be overwritten.
 * A suggested initial challenge is to build a brick, then a line of them, then a wall, then a cube, then a hollow cube, then a pyramid... Be careful when building cubes, it is easy to get carried away! A cube of just side 100 would have a million bricks.