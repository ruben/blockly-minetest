#!/usr/bin/python

#    Copyright (C) 2015-2016  Ruben Rodriguez <ruben@gnu.org>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

import os, sys, time, ctypes, re, time
from gi.repository import Gtk, Gdk, WebKit
from gi.repository import Wnck

def get_html():
	dom = view.get_dom_document()
	print( html )
	return html

scriptheader="""
try:
  os.delete('/dev/shm/minetest-message')
except: pass
f = open('/dev/shm/minetest-message-tmp', 'aw')
f.write("/time 6000\\n")
f.write("/grant singleplayer fly\\n")
f.write("/grant singleplayer fast\\n")
def createblock(material, x, y, z):
    f = open('/dev/shm/minetest-message-tmp', 'aw')
    f.write("/createblock %s %d %d %d\\n" % (material, x, y, z))
"""

scriptfooter="""
f.close()
os.rename('/dev/shm/minetest-message-tmp', '/dev/shm/minetest-message')
"""

scriptheaderslow="""
import time, os

try:
  os.delete('/dev/shm/minetest-message')
except: pass

while os.path.isfile('/dev/shm/minetest-message'):
  time.sleep(0.001)

f = open('/dev/shm/minetest-message', 'aw')
f.write("/time 6000\\n")
f.write("/grant singleplayer fly\\n")
f.write("/grant singleplayer fast\\n")
f.close()

def createblock(material, x, y, z):
  while os.path.isfile('/dev/shm/minetest-message'):
    time.sleep(0.001)
  f = open('/dev/shm/minetest-message', 'aw')
  f.write("/createblock %s %d %d %d\\n" % (material, x, y, z))
  f.close()

"""


def call_javascript( script ):
	'''
	return the result of the script
	'''
	#view.execute_script('document.title=document.documentElement.innerHTML;')
	view.execute_script('document.title=%s;' %script)
	frame = view.get_main_frame()
	result = frame.get_title()
        result = re.sub('__newline__', '\n',result)
        result = re.sub('__space__', ' ',result)
	return result

def execute_python( script ): 
    script= scriptheader + script + scriptfooter
    print script
    exec( script )

def execute_python_slow( script ): 
    script= scriptheaderslow + script
    print script
    exec( script )

def destroy(widget, data=None):
    Gtk.main_quit()

def plug_event(widget):
    print("A plug has been inserted")

def convert_to_python(widget, text):
    script="Blockly.Python.workspaceToCode()"
    view.execute_script('document.title=%s;' %script)
    frame = view.get_main_frame()
    result = frame.get_title()
    result = re.sub('__newline__', '\n',result)
    result = re.sub('__space__', ' ',result)
    print(result)
    textbuffer = text.get_buffer()
    textbuffer.set_text(result)


def run_python(widget, text):
    textbuffer = text.get_buffer()
    text = textbuffer.get_text(textbuffer.get_start_iter() , textbuffer.get_end_iter(), 0)
    print text
    exec (scriptheader + text + scriptfooter)

def run_python_slow(widget, text):
    textbuffer = text.get_buffer()
    text = textbuffer.get_text(textbuffer.get_start_iter() , textbuffer.get_end_iter(), 0)
    print text
    exec (scriptheaderslow + text + scriptfooter)


################## Test WebKitGTK ###################
view = WebKit.WebView()
print(view)


"""settings = WebKit.web_settings_new()
for prop in 'enable-webaudio enable-file-access-from-file-uris enable-universal-access-from-file-uris enable-developer-extras enable-accelerated-compositing enable-webgl'.split():
	gval = glib.GValue(True)
	glib.g_object_set_property( settings, prop, gval )
view.set_settings( settings )"""

view.load_uri( 'file://%s/blockly.html'%os.path.abspath('.'))

win = Gtk.Window()
win.connect("destroy", destroy)

root = Gtk.VBox()

notebook=Gtk.Notebook()

header = Gtk.HBox()
root.pack_start( header, expand=False , fill=False, padding=0)

notebook.insert_page(root, Gtk.Label('Blockly'), 1)

textscroll=Gtk.ScrolledWindow()
text=Gtk.TextView()
textscroll.add(text)
textroot=Gtk.VBox()
textheader=Gtk.HBox()
button2 = Gtk.Button('Run!')
button2.connect('clicked', run_python, text )
textheader.pack_start( button2, expand=False, fill=False, padding=0 )
button2 = Gtk.Button('Run block by block!')
button2.connect('clicked', run_python_slow, text )
textheader.pack_start( button2, expand=False, fill=False, padding=0 )
textroot.pack_start( textheader, expand=False , fill=False, padding=0)
textroot.pack_start( textscroll, expand=True , fill=True, padding=0)

notebook.insert_page(textroot, Gtk.Label('Python Code'), 2)
win.add( notebook )

button = Gtk.Button('Run!')
button.connect('clicked', lambda b: execute_python(call_javascript("Blockly.Python.workspaceToCode()")) )
header.pack_start( button, expand=False, fill=False, padding=0 )

button = Gtk.Button('Run block by block!')
button.connect('clicked', lambda b: execute_python_slow(call_javascript("Blockly.Python.workspaceToCode()")) )
header.pack_start( button, expand=False, fill=False, padding=0 )

button = Gtk.Button('Convert to Python!')
button.connect('clicked', convert_to_python, text )
header.pack_start( button, expand=False, fill=False, padding=0 )

scroll=Gtk.ScrolledWindow()

scroll.add( view)
root.pack_start( scroll, expand=True, fill=True, padding=0)

win.set_default_size( 1024, 700 )

win.show_all()
Gtk.main()

