Blockly.Blocks['minetest_createblock'] = {
  init: function() {
    this.setHelpUrl('http://www.example.com/');
    this.setColour(45);
    this.appendValueInput("material")
        .setCheck("String")
        .appendField("material");
    this.appendValueInput("x")
        .setCheck("Number")
        .appendField("x");
    this.appendValueInput("y")
        .setCheck("Number")
        .appendField("y");
    this.appendValueInput("z")
        .setCheck("Number")
        .appendField("z");
    this.setInputsInline(true);
    this.setPreviousStatement(true, "null");
    this.setNextStatement(true, "null");
    this.setTooltip('Create a minetest block of a given material, at coordinates x, y, z');
  }
};



Blockly.Python['minetest_createblock'] = function(block) {
  var value_material = Blockly.Python.valueToCode(block, 'material', Blockly.Python.ORDER_ATOMIC);
  var value_x = Blockly.Python.valueToCode(block, 'x', Blockly.Python.ORDER_ATOMIC);
  var value_y = Blockly.Python.valueToCode(block, 'y', Blockly.Python.ORDER_ATOMIC);
  var value_z = Blockly.Python.valueToCode(block, 'z', Blockly.Python.ORDER_ATOMIC);
  if (value_material == "") value_material = "air";
  value_material = value_material.replace(/[\(]/,'');
  value_material = value_material.replace(/[\)]/,'');
  if (value_x == "") value_x = 0;
  if (value_y == "") value_y = 0;
  if (value_z == "") value_z = 0;
  var code = 'createblock("'+value_material+'", '+ value_x +', '+ value_y + ', '+ value_z +')\n';
  return code;
};



Blockly.Blocks['minetest_materials'] = {
  init: function() {
    this.setHelpUrl('http://www.example.com/');
    this.setColour(45);
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([
["Air", "air"],
["Bookshelf", "bookshelf"],
["Brick", "brick"],
["Dirt", "dirt"],
["Glass", "glass"],
["Stone", "stone"],
["Wood", "wood"],
["Gold", "default:goldblock"],
["Nyan", "default:nyancat"],
["Diamond", "default:diamondblock"],
]), "MATERIAL");
    this.setOutput(true, "String");
    this.setTooltip('List of materials for Minetest blocks');
  }
};

Blockly.Python['minetest_materials'] = function(block) {
  code = block.getFieldValue('MATERIAL');
  return [code, Blockly.Python.ORDER_NONE];
};

