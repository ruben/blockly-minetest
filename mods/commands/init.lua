-- External Command (external_cmd) mod
-- Allows server commands / chat from outside minetest
--
-- Copyright 2013 Menche <menche_mt@yahoo.com>
-- Copyright 2015 Ruben Rodriguez <ruben@gnu.org>
-- 
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

local admin = minetest.setting_get("name")

if admin == nil then
	admin = "SERVER"
end

minetest.register_globalstep(
	function(dtime)
		f = (io.open("/dev/shm/minetest-message", "r"))
		if f ~= nil then
			f:close()
			for line in io.lines('/dev/shm/minetest-message') do
				local message = line
	
				if message ~= nil then
					local player = minetest.get_player_by_name("singleplayer")
					local cmd, param = string.match(message, "^/([^ ]+) *(.*)")
					if not param then
						param = ""
					end
					local cmd_def = minetest.chatcommands[cmd]
					if cmd_def then
						cmd_def.func("singleplayer", param)
					else
						minetest.chat_send_all(admin..": "..message)
					end
				end
			end
			os.remove("/dev/shm/minetest-message")
		end
	end
)

minetest.register_chatcommand("createblock", {
        params = "material x y z",
        description = "Creates block of <material> at (<x>, <y>, <z>)",
        func = function(name, param)
                local found, _, material, xx, yy, zz = param:find("^(.*)%s+([+-]?%d+)%s+([+-]?%d+)%s+([+-]?%d+)$")
                if found == nil then
                        return
                end
                xx=tonumber(xx)
		yy=tonumber(yy)
		zz=tonumber(zz)
		minetest.add_node({ x = xx, y = yy, z = zz}, { name = material })
        end,
})

minetest.register_chatcommand("whereami", {
        description = "Prints the player coordinates",
        func = function(name)
		local player = minetest.get_player_by_name("singleplayer")
                local pos=player:getpos()
		minetest.chat_send_all("X:"..math.floor(pos.x).." Y:"..math.floor(pos.y).." Z:"..math.floor(pos.z))
        end,
})
